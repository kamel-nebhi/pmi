# takes as input such file and returns the top 10 most frequent event types that occur within a predefined time window
# read csv file create a sqlite3 db and then create a function for the query

import pandas as pd
import sqlite3


def dbCreation():
    # init chunk size for big file
    chunksize = 300000
    i=0
    # create and connect to the DB
    con = sqlite3.connect("pmi_data_utf8.db") # database file input
    cur = con.cursor()
    # specify the UTF-8 encoding for the DB
    cur.executescript("""
        PRAGMA encoding="UTF-8";
    """)
    # create the table
    cur.executescript("""
        DROP TABLE IF EXISTS pmi_data;
        CREATE TABLE pmi_data (uniqueid VARCHAR(255), eventtype TEXT, dateandtime DATETIME DEFAULT CURRENT_TIMESTAMP);
    """)
    con.commit()

    # split the merge dataset into chunk of 300000 to avoid any memory issue and append to sqlite DB
    for chunk in pd.read_csv("data/test.csv",sep=',',chunksize=chunksize, parse_dates=[1], infer_datetime_format=True, error_bad_lines=False, names=['uniqueid', 'eventtype','dateandtime']):
        chunk.to_sql('pmi_data', con, if_exists='append', index=False)
        i += 1
        print("chunk ", i, " append to table pmi_data")
    con.close()


def requestDB(start_date,end_date):
    con = sqlite3.connect("pmi_data_utf8.db") # database file input
    cur = con.cursor()
    cur.execute("SELECT eventtype, count(eventtype) FROM pmi_data WHERE strftime('%Y-%m-%d', dateandtime) > strftime('%Y-%m-%d',?) AND strftime('%Y-%m-%d', dateandtime) < strftime('%Y-%m-%d',?)  GROUP BY eventtype ORDER BY eventtype DESC" , (start_date,end_date,))
    print(cur.fetchall())
    con.close()


if __name__ == '__main__':
    dbCreation()
    requestDB('2017-04-01','2017-09-01')